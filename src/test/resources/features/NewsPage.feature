
Feature: News page feature


  Scenario: user should open the news page and see the title correctly
    Given user on the homepage
    When user clicks on the any news
    Then user should see article name as a title

  Scenario: user should be able see all the links in the news page
    Given user on the homepage
    When user clicks on the any news
    Then user should see links on the news page