#it is written for the case 1.


Feature: Contact form

 Background:
   Given user on the contact form page

  Scenario Outline: User should not input invalid first name
    When user inputs <firstname> to the "firstname"
    Then it should give an error message for the "firstname"
    Examples: #invalid inputs here
  #like more than 50 char or special or numbers

  Scenario Outline: User should not input invalid last name
    When user inputs <lastname> to the "lastname"
    Then it should give an error message for the "lastname"
    Examples: #invalid inputs here
#like more than 50 char or special or numbers

  Scenario Outline: User should not able to input invalid email
    When user inputs <emailaddress> to the "emailaddress"
    Then it should give an error message for the "email"
    Examples: #invalid inputs here

  Scenario Outline: User should not able to input invalid phone number
    When user inputs <phonenumber> to the "phonenumber"
    Then it should give an error message for the "phonenumber"
    Examples: #invalid inputs here

  Scenario: User should see Body wrap up as default selection
    Then user should see "Body Wrap"

  Scenario: User should see all of the four option under services
    When user clicks on the services
    Then user should see all of the options

  Scenario Outline: User should select any of the services
    When user clicks on the services
    And user selects <service>
    Then user should see <service>

    Examples: #give inputs here

  Scenario: User should select one service at a time
    When user clicks on the services
    And user selects more than one services
    Then it should show only one service as selected

  Scenario: User should see current time
    When user clicks on the date
    Then user should see current date

  Scenario: User should not choose time before today
    When user clicks on the date
    And user selects invalid date
    Then it should give an error message for the "date"

  Scenario: User should select only weekdays
    When user clicks on the date
    And user selects weekend dates
    Then it should give an error message for the "date"

  Scenario: User should not select invalid hours
    And user clicks on the date
    And user select valid date
    And user clicks on hours
    When user selects invalid hours
    Then it should give an error message for the "hours"

  Scenario: User should see 10 am as a default
    When user clicks on the date
    And user selects valid date
    Then time selection menu should show "10"

  Scenario: User should select valid hours
    And user clicks on the date
    And user select valid date
    And user clicks on hours
    When user selects valid hours
    Then user should see selected hours

 Scenario Outline: User should select every hour in weekdays
   And user clicks on the date
   And user select valid date
   And user clicks on hours
   When user selects <hours>
   Then user should see selected hours
   Examples: #inputs here

  Scenario: User should not able to add more than 500 characters to the comment field
    When user inputs invalid comments
      | long comment here +500 |
    Then it should give an error message for the "comments"


  Scenario: User should submit form without comment
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the services
    And user selects "services"
    And user clicks on the date
    And user selects valid date
    And user clicks on hours
    And user selects valid hours
    When user clicks on the submit button
    Then user should see success message

  Scenario: User should submit form with comment
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the services
    And user selects "services"
    And user clicks on the date
    And user selects valid date
    And user clicks on hours
    And user selects valid hours
    And user inputs "somecomments"
    When user clicks on the submit button
    Then user should see success message

  Scenario: User should see message with checkbox without comment
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the services
    And user selects "services"
    And user clicks on the date
    And user selects valid date
    And user clicks on hours
    And user selects valid hours
    And user selects send an update checkbox
    When user clicks on the submit button
    Then user should see success message
    And user should see following message
      |You will receive the latest updates on our special offers and campaigns.|

  Scenario: User should see message with checkbox and comment
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the services
    And user selects "services"
    And user clicks on the date
    And user selects valid date
    And user clicks on hours
    And user selects valid hours
    And user selects send an update checkbox
    And user inputs "somecomments"
    When user clicks on the submit button
    Then user should see success message
    And user should see following message
      | You will receive the latest updates on our special offers and campaigns. |

  Scenario: User should not submit form without selecting date
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the services
    And user selects "services"
    When user clicks on the submit button
    Then it should give an error message for the "date"

  Scenario: User should not submit form without selecting service
    And user inputs "firstname" to the "firstname"
    And user inputs "lastname" to the "lastname"
    And user inputs "emailaddress" to the "emailaddress"
    And user inputs "phonenumber" to the "phonenumber"
    And user clicks on the date
    And user selects valid date
    And user clicks on hours
    And user selects valid hours
    When user clicks on the submit button
    Then it should give an error message for the "service"