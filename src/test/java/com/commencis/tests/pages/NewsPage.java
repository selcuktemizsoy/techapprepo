package com.commencis.tests.pages;

import com.commencis.tests.utilities.Driver;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NewsPage {
    public NewsPage(){
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy(css = "h1.article__title")
    public WebElement title;

    @FindBy(css = "div.article-content a")
    public List<WebElement> linksOfTheArticle;

    public void checkTheLinkIsNotNull(){
        for (WebElement webElement : linksOfTheArticle) {
            Assert.assertFalse(webElement.getAttribute("href").equals(""));
        }
    }
    
    public void verifyTitle(){
    
     Assert.assertTrue(Driver.get().getTitle().contains(title.getText()));
     
    }
}
