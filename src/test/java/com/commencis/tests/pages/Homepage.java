package com.commencis.tests.pages;

import com.commencis.tests.utilities.BrowserUtils;
import com.commencis.tests.utilities.Driver;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Homepage {
    public Homepage(){
        PageFactory.initElements(Driver.get(), this);
    }

    @FindBy (xpath = "//article[@class='post-block post-block--image post-block--unread']")
    public List<WebElement> listOfArticle;

    public void checkTheAuthorOfArticle(){
        for(WebElement element : listOfArticle){
            Assert.assertFalse(element.findElement(By.className("river-byline__authors")).getText().equals(""));
        }
    }
    public void checkTheImageOfArticle(){

        for(WebElement element : listOfArticle){
            Assert.assertFalse(element.findElement(By.tagName("img")).getAttribute("src").equals(""));
        }
    }

    public void clickAnyNews(){
        Random random = new Random();
        int rndNumber = random.nextInt(listOfArticle.size());
        listOfArticle.get(rndNumber).click();
    }
}
