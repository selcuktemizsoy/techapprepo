package com.commencis.tests.step_definitions;

import com.commencis.tests.pages.Homepage;
import com.commencis.tests.pages.NewsPage;
import com.commencis.tests.utilities.Driver;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class NewsStepDef {

    @When("user clicks on the any news")
    public void user_clicks_on_the_any_news() {
        Homepage homepage = new Homepage();
        homepage.clickAnyNews();
    }

    @Then("user should see article name as a title")
    public void user_should_see_article_name_as_a_title() {
        NewsPage newsPage = new NewsPage();
       newsPage.verifyTitle();
    }

    @Then("user should see links on the news page")
    public void user_should_see_links_on_the_news_page() {
        NewsPage newsPage = new NewsPage();
        newsPage.checkTheLinkIsNotNull();
    }
}
