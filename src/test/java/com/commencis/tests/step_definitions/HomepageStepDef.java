package com.commencis.tests.step_definitions;

import com.commencis.tests.pages.Homepage;
import com.commencis.tests.utilities.ConfigurationReader;
import com.commencis.tests.utilities.Driver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

public class HomepageStepDef {
    @Given("user on the homepage")
    public void user_on_the_homepage() {
        String url = ConfigurationReader.get("url");
        Driver.get().get(url);
    }

    @Then("user should see every article has an author")
    public void user_should_see_every_article_has_an_author() {
        Homepage homepage = new Homepage();

        homepage.checkTheAuthorOfArticle();
    }

    @Then("user should see every article has an image")
    public void user_should_see_every_article_has_an_image() {
        Homepage homepage = new Homepage();

        homepage.checkTheImageOfArticle();
    }
}
