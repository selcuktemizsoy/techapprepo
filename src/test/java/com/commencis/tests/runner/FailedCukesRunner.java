package com.commencis.tests.runner;


import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"html:target/failed-html-reports"},
        glue = "com/commencis/tests/step_definitions",
        features = "@target/rerun.txt"
)
public class FailedCukesRunner {
}
