package com.commencis.tests.runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/*
to get an cucumber plugin html report please run the command on the project folder
mvn verify
or
mvn test
 */

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"json:target/cucumber.json",
        "rerun:target/rerun.txt"},
        glue = "com/commencis/tests/step_definitions",
        features = "src/test/resources/features",
        dryRun = false,
        tags = ""
)
public class CukesRunner {
}
