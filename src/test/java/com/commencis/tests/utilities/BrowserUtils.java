package com.commencis.tests.utilities;

import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class BrowserUtils {

    public static List<String> webElementToString(List<WebElement> listOfElements){
        List <String> textOfElements = new ArrayList<>();
        for(WebElement element : listOfElements){
            textOfElements.add(element.getText());
        }
        return textOfElements;
    }
}
